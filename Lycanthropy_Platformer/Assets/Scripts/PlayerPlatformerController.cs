﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerPlatformerController : PhysicsObject
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    System.Random random;
    public bool howl = false;
    public bool howlCooldown = false;
    float howlDuration = 0;
    public GameObject grid;
    public int howlsLeft = 3;
    

    // Start is called before the first frame update
    void Start()
    {
        random = new System.Random();
    }

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
            Vector2 move = Vector2.zero;
            if (howl)
            {
                howlDuration += Time.deltaTime;
                velocity.y = 1;
                if (howlDuration >= 3)
                {
                    howl = false;
                    grid.GetComponent<TilemapCollider2D>().enabled = true;
                    howlDuration =0;
                    howlCooldown = true;
                    //howlsLeft -=1;
                }
            }
            if (howlCooldown)
            {
                howlDuration += Time.deltaTime;
                if (howlDuration >= 5)
                {
                    howlCooldown = false;
                    howlDuration = 0;
                }
            }
            if ((Input.GetAxis("Fire1")==1)&&(!howlCooldown))
            {
                howl = true;
                grid.GetComponent<TilemapCollider2D>().enabled = false;
                velocity.y = 1;
            }
            if (!howl)
            {
                move.x = Input.GetAxis("Horizontal");

                if (Input.GetButtonDown("Jump")&& grounded)
                {
                    velocity.y = jumpTakeOffSpeed;
                }
                else if (Input.GetButtonUp("Jump"))
                {
                    if (velocity.y > 0)
                    {
                        velocity.y = velocity.y * .5f;
                    }
            }
            }

            targetVelocity = move * maxSpeed;

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        print("collision");
        if (other.gameObject.CompareTag ("Person"))
        {
            other.gameObject.GetComponent<PersonToPuppy>().SpawnPuppy();
        }
    }
}
