﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController2 : PhysicsObject
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    System.Random random;
    int timer = 50;
    

    // Start is called before the first frame update
    void Start()
    {
        random = new System.Random();
    }

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
        timer += 1;
            Vector2 move = Vector2.zero;
            int variantSpeed = random.Next(4) +5;
            if (timer >= 200){
                maxSpeed = variantSpeed;
                timer = 0;
            }
            double speedAdjuster = variantSpeed*0.1;

            move.x = Input.GetAxis("Horizontal")*variantSpeed*(float)0.1;

            if (Input.GetButtonDown("Jump")&& grounded)
            {
                velocity.y = jumpTakeOffSpeed;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * .5f;
                }
            }

            targetVelocity = move * maxSpeed;

    }
}
