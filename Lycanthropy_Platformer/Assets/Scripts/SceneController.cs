﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime;

public class SceneController : MonoBehaviour
{
    private static string [] Levels = {"MainMenu", "GameOver", "SampleScene"};
    private static int LevelIndex = 0;
    public float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Gameover()
    {
        LevelIndex = 1;
        SceneManager.LoadScene(Levels[LevelIndex]);
    }
    public static void Mainmenu()
    {
        LevelIndex = 0;
        SceneManager.LoadScene(Levels[LevelIndex]);
    }
    public void Levelstart()
    {
        LevelIndex = 2;
        SceneManager.LoadScene(Levels[LevelIndex]);
    }
}
