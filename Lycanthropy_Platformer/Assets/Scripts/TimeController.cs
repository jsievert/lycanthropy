﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
    // Start is called before the first frame update
    public float timer;
    public Text timerText;
    public GameObject scenehandler;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //take the timer and add the time since the last frame.
            timer -= Time.deltaTime;
            //calls a function that will set the timer text.
            SetTimerText();
            if (timer <= 0)
            {
                scenehandler.GetComponent<SceneController>().Gameover();
            }
    }
    //This function will update the Timer Text when called.
    void SetTimerText()
    {
        //in order to make sure we have no errors due to the float occasionally being less than 4 long we call this if statement.
        if(timer.ToString().Length>4)
        {
            if(timer<=60)
            {
                //when the timer variable is longer than length of 4, we take only the first four characters, so that it has a nice format for the UI.
                timerText.text = "Timer: 0:"+timer.ToString().Substring(0,4);
                
            }
            else
            {
                int minutes = (int) Mathf.Floor((timer/60));
                float seconds = timer - (minutes*60);  
                timerText.text = "Timer: "+minutes.ToString()+":"+seconds.ToString().Substring(0,4);
            }
        //when the time is already four characters or less...
        }
        else
        {
            if(timer<=60)
            {
                //when the timer variable is longer than length of 4, we take only the first four characters, so that it has a nice format for the UI.
                timerText.text = "Timer: 0:"+timer.ToString();
                
            }
            else
            {
                int minutes = (int) Mathf.Floor((timer/60));
                float seconds = timer - (minutes*60);  
                timerText.text = "Timer: "+minutes.ToString()+":"+seconds.ToString();
            }
        }
    }
}
