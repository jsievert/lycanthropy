﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryPuppiesController : PhysicsObject
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    System.Random random;
    public GameObject mainPuppy;
    int timer = 0;
    bool howl = false;
    

    // Start is called before the first frame update
    void Start()
    {
        random = new System.Random();
    }

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
            if (mainPuppy.GetComponent<PlayerPlatformerController>().howl)
            {
                howl = true;
            }
            else
            {
                howl = false;
            }
            timer += 1;
            Vector2 move = Vector2.zero;
            if (timer >= 200){
                int variantSpeed = random.Next(4) +5;
                maxSpeed = variantSpeed;
                timer = 0;
            }
            if (((mainPuppy.transform.position.x- this.transform.position.x) > 3)||(howl&&(mainPuppy.transform.position.x > this.transform.position.x))) 
            {
                move.x = 1;
            }
            else if (((mainPuppy.transform.position.x- this.transform.position.x) < -3)||(howl&&(mainPuppy.transform.position.x < this.transform.position.x)))
            {
                move.x = -1;
            }
            else
            {
                move.x = Input.GetAxis("Horizontal");
            }
            if (((mainPuppy.transform.position.y- this.transform.position.y) > 1.7&&grounded)||(howl&&(mainPuppy.transform.position.y > this.transform.position.y)))
            {
                velocity.y = jumpTakeOffSpeed;
            }
            else if ((mainPuppy.transform.position.y- this.transform.position.y) > 1.7)
            {
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * .5f;
                }
            }
            else if (Input.GetButtonDown("Jump")&& grounded)
            {
                velocity.y = jumpTakeOffSpeed;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * .5f;
                }
            }

            targetVelocity = move * maxSpeed;

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        print("collision");
        if (other.gameObject.CompareTag ("Person"))
        {
            other.gameObject.GetComponent<PersonToPuppy>().SpawnPuppy();
        }
    }
}
